# Overview

A skeleton [APT/PPA][0] repository for the [MGI's][1] data processing pipelines for [CCDG][2].  The packages are targeted towards [Ubuntu 14.04][3].

# Usage

## production mode

Copy the debian package you want to add to the repo into the top-level directory of this repository and then run these docker commands:

    docker build -t apt:v1 .
    docker run -i -t -v $PWD:/build --rm apt:v1 <package.deb>

Aftewards, do the following:

1.  delete the debian package you copied into the top-level directory of this repository
2.  Add and commit the remaining contents of this directory into the git repository.

[0]: https://wiki.debian.org/HowToSetupADebianRepository
[1]: http://genome.wustl.edu
[2]: https://www.genome.gov/27563570/
[3]: http://releases.ubuntu.com/14.04/
